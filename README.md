# README #

IT Crowd.
Complete following code challenge.


## Problem:

A new company, called “scatchbling” (SB) was formed.  SB would like to have customers and other clients access an API to list all of the back scratchers that they are selling on the market.   In order to complete this, you, the developer, must create a simple RESTful interface that will provide access to the company’s database. 

A simple example of a backscratcher for sale is:

#### Name |  Description | Size | Price
“The Itcher” | “Scratch any itch” | “XL” | $27.00
“The Blinger” | “Diamonds” | “L” | $343.00
“Glitz and Gold” | “Gold handle and fancy emeralds make this shine” | “XL,L,M,S” | $4343.00

### Technical Details:

Deployment:  Please create a free test application using Heroku
Database:  Create a simple PostGreSQL database on Heroku.  You will need to create a table with the following items.  item_name, item_description, item_cost

### Deliverables:

Working API - Should send link to a proper RESTful API. API should include:
●	Read
●	Write
●	Create
●	Update

1.	Django Admin login and password 
2.	Link with Heroku project
3.	Sample REST Calls Documented for each call using Postman.
4.	Committed to your github account
5.	Screenshots of rest call results


### Extra Credit:
Enable tokens API access.
User interface to see items.


# Solution:

### How do I get set up? ###

<TBD>

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

<TBD>

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

<TBD>

* Repo owner or admin
* Other community or team contact