from django.db import models
from django.utils.translation import ugettext_lazy as _


class Item(models.Model):
    """
    Item to sell
    """
    item_name = models.CharField(_('Name'), max_length=100)
    item_description = models.TextField(_('Description'), max_length=500,
                                        blank=True, null=True)
    item_cost = models.DecimalField(_('Cost'), max_digits=10, decimal_places=2,
                                    blank=True, null=True)
    size = models.CharField(_('Size'), blank=True, null=True, max_length=10)
