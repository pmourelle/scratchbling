from django.shortcuts import render

from rest_framework import viewsets

from .models import Item
from .serializers import ItemSerializer


class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Item.objects.all().order_by('item_name')
    serializer_class = ItemSerializer


def simple_items_list(request):
    items = Item.objects.all()
    return render(request, 'item_list.html', {
        'items': items,
    })
